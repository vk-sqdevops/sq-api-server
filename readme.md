# Api Server Image Creator

## Overview

* Using a vanilla instance of Windows Server 2016 Base launched on AWS 
* This repository installs and configures required components to prepare an AMI image
* Configures IIS Default web site to point to the custom folder

## Setup

1. Provision a new instance of Windows Server 2016 Base
1. Login into the new instance using Rdp and Administrator account
1. Open powershell as an Administrator
1. Execute the following statements:
```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Set-ExecutionPolicy Bypass -Scope Process -Force

# Install Chocolately
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install git
choco install git -params '"/GitOnlyOnPath"' -y

# Disable pop-up
& 'C:\Program Files\Git\bin\git.exe' config --global credential.modalPrompt false

# Clone The Repo
& 'C:\Program Files\Git\bin\git.exe' clone --depth=1 https://bitbucket.org/vk-sqdevops/sq-api-server.git "C:\Squirt"

cd "C:\Squirt"
& ./setup.ps1

```
1. Create custom AMI Image for automated deployment and auto-scaling group: https://aws.amazon.com/premiumsupport/knowledge-center/sysprep-create-install-ec2-windows-amis/
1. Create an AMI from the shut-down instance
1. Note the AMI id to use in the terraform setup
1. Repeat the process whenever a new version of the web site or system components is released
