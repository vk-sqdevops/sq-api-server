﻿<%@Page Language="C#" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="ICSSolutions.Squirt.Global" %>
<%@ Import Namespace="SquirtMobile.Core.Services" %>

<!DOCTYPE html>     
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Health Check</title>
    <meta name="robots" content="noindex" /> 
</head>
    
     <script language="c#" runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {

            
        try
        {
            
        
            using (var connection = new SqlConnection(GeneralCache.ConnectionString))
            {
                var query = "select 1";
                
                var command = new SqlCommand(query, connection);
                connection.Open();
                
                command.ExecuteScalar();
                
                
                lblHealth.InnerText = "Server Healthy";

            }
        }

    catch (Exception ex)
    {

          NLogService logService = new NLogService();
        
        lblHealth.InnerText = "ex.Message";
        
        logService.Fatal("Fatal " + ex.Message);
    }   



                   

        }
        
        
        

		
</script>


<body>
    <form id="form1" runat="server">
    <div>
       
       <label id ="lblHealth"  runat="server"></label>
    
    </div>
    </form>
</body>
</html>