$features = (
  "web-server",
  "web-mgmt-console",
  "web-scripting-tools",
  "web-net-ext",
  "web-net-ext45",
  "web-asp",
  "web-asp-net",
  "web-asp-net45",
  "web-isapi-ext",
  "web-isapi-filter",
  "web-default-doc",
  "web-dir-browsing",
  "web-http-errors",
  "web-http-redirect",
  "web-static-content",
  "web-http-logging",
  "web-log-libraries",
  "web-http-tracing",
  "web-basic-auth",
  "web-digest-auth",
  "web-filtering",
  "web-url-auth",
  "web-windows-auth"
)

# Install windows features
Install-WindowsFeature -Name $features

# Validate Windows Features
$status = Get-WindowsFeature -Name $features | where {$_.Installed -eq "True"} | select Name -ExpandProperty Name
if($status){
  $errors += "Windows features did not install: $status"
}

# Install url rewrite module
choco install urlrewrite -y

# Reconfigure Default Web Site path
net stop w3svc
Import-Module WebAdministration
Set-ItemProperty 'IIS:\Sites\Default Web Site\' -name physicalPath -value (Resolve-Path "./Api").Path
net start w3svc